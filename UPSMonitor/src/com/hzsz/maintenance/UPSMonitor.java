package com.hzsz.maintenance;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fazecast.jSerialComm.SerialPort;

public class UPSMonitor {
	
	private static final byte CMD_GET_STATE[] = {0x01, 0x03, 0x2A, -0x08, 0x00, 0x01, 0x0D, -0x1D};
	//private static final byte CMD_GET_STATE[] = {0x01, 0x78, 0x2A, -0x08, 0x00, 0x01, 0x0D, -0x1D};
	
	private static Logger logger = LoggerFactory.getLogger(UPSMonitor.class);
	
	private String m_IPAddr = "";
	
	//private int m_State[]; 
	private String m_CommPort[];
	
	//连续中断的最大次数，超过后进行相应操作。
	private int m_WarnCount = 30;

	public static void main(String[] args) {
		
		
		//解析参数
		if (args.length != 3) {
			usage();
			return;
		}
		
		UPSMonitor um = new UPSMonitor(args[0], args[1], args[2]);
		um.run();
		
		return;
		
	}
	
	public static void usage() {
		System.out.println("Usage: UPSMonitor <com port list> <max_failure> <ip addr>");
		System.out.println("\t<com port list> /dev/ttyXRUSB1,/dev/ttyXRUSB2");
		System.out.println("\t<max_failure> default is 30");
		System.out.println("\t<ip addr> ex. 10.0.16.189/24");
	}
	
	public UPSMonitor(String CommPorts, String interval, String ipaddr) {
		m_CommPort = CommPorts.split(",");
		//m_State = new int[m_CommPort.length];
		m_WarnCount = Integer.parseInt(interval);
		m_IPAddr = ipaddr;
		setAlert(false);
	}
	
	public void run() {
		int i;
		boolean f;
		boolean lastState;
		UPSCheck uc[] = new UPSCheck[m_CommPort.length];
		
		logger.info("Start monitoring ups ...");
		logger.info("Ports: " + String.join(",", m_CommPort));
		logger.info("Max failure count: " + m_WarnCount);
		logger.info("IP Addr: " + m_IPAddr);
		
		SerialPort sp[] = SerialPort.getCommPorts();
		for (i=0;i<sp.length;i++) {
			logger.info("Comm port: " + sp[i].getSystemPortName());
		}
		
		for (i=0;i<m_CommPort.length;i++) {
			uc[i] = new UPSCheck(i);
			uc[i].setName(m_CommPort[i]);
			uc[i].start();
		}
		
		//循环监测各个线程的连续中断次数，并进行处理。
		lastState = false;
		while (true) {
			f = false;
			for (i=0;i<m_CommPort.length;i++) {
				if (uc[i].getCheckState() > m_WarnCount) {
					f = true;
				}
			}
			
			if (f) {
				if (!lastState) {
					lastState = true;
					
					//报警操作
					setAlert(true);
					logger.warn("Alert!");
				}
			} else {
				if (lastState) {
					lastState = false;

					//取消报警操作
					setAlert(false);
					logger.info("Alert cleared!");
				}
			}
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				return;
			}
		}
		
		
	}
	
	class UPSCheck extends Thread{
		private int m_Index;
		
		//累计连续失败数量
		private int m_State = 0;
		
		public UPSCheck(int idx) {
			m_Index = idx;
		}
		
		public synchronized int getCheckState() {
			return m_State;
		}
		
		public synchronized void incCheckState() {
			m_State ++;
		}
		
		public synchronized void resetCheckState() {
			m_State = 0;
		}
		
		
		public void run() {
			byte buf[] = new byte[8];
			int c;
			int v;
			String errstr;
			InputStream sin = null;
			OutputStream sout = null;
			
			SerialPort sp;

			logger.info("Checking ups: " + m_CommPort[m_Index]);
			//SerialPort comPort[] = SerialPort.getCommPorts();
			sp = SerialPort.getCommPort(m_CommPort[m_Index]);
			//SerialPort sp = SerialPort.getCommPorts()[0];
			sp.setComPortParameters(9600, 8, SerialPort.ONE_STOP_BIT, SerialPort.NO_PARITY);
			sp.openPort();
			//sp.setComPortTimeouts(SerialPort.TIMEOUT_READ_SEMI_BLOCKING, 1000, 0);
			sp.setComPortTimeouts(SerialPort.TIMEOUT_READ_BLOCKING, 1000, 0);
			//System.out.println("Serial port " + sp.getDescriptivePortName());
			while (true) {
				
				try {
					sp = SerialPort.getCommPort(m_CommPort[m_Index]);
					sp.setComPortParameters(9600, 8, SerialPort.ONE_STOP_BIT, SerialPort.NO_PARITY);
					sp.openPort();
					sp.setComPortTimeouts(SerialPort.TIMEOUT_READ_BLOCKING, 1000, 0);

					sin = null;
					sout = null;
					
					sin = sp.getInputStream();
					sout = sp.getOutputStream();
					sout.write(CMD_GET_STATE);
					c = sin.read(buf);
					if (c==7) {
						v = buf[3];
						if (v < 0) {
							v = 256 + v;
						}
						v = v * 256;
						if (buf[4] < 0) {
							v += buf[4] + 265;
						} else {
							v += buf[4];
						}
						//System.out.println("Input voltage is " + v/10.0 + "V.");
						logger.debug("Input voltage is " + v/10.0 + "V.");
						if (v < 2000) {
							incCheckState();
							logger.warn("Low power!");
						} else {
							resetCheckState();
						}
					} else {
						incCheckState();
						//System.out.println("Data error! len=" + c);
						errstr = "";
						for (int i=0;i<c;i++) {
							System.out.print(buf[i] + ",");
							errstr = buf[i] + ",";
						}
						logger.warn("Data error! len=" + c + ": " + errstr);
					}
				} catch (Exception e) {
					logger.warn(e.getMessage(), e);
					incCheckState();
				} finally {
					try {
						if (sin != null) {
							sin.close();
						}
					} catch (IOException e) {
						logger.warn(e.getMessage(), e);
					}
					try {
						if (sout != null) {
							sout.close();
						}
					} catch (IOException e) {
						logger.warn(e.getMessage(), e);
					}
				}
				
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					logger.warn("exit: ", e);
					return;
				}
			
			}
		}
	}
	
	//设置或取消报警操作。
	public void setAlert(boolean alt) {
		//TODO
		//ip addr add 10.0.16.189/24 dev eth0
		//ip addr del 10.0.16.189/24 dev eth0
		if (alt) {
			//ip addr del 10.0.16.189/24 dev eth0
			try {
				Runtime.getRuntime().exec(new String[] {"ip", "addr", "del", m_IPAddr, "dev", "eth0"});
				logger.info("Delete ip: " + m_IPAddr);
			} catch (IOException e) {
			}
		} else {
			//ip addr add 10.0.16.189/24 dev eth0
			try {
				Runtime.getRuntime().exec(new String[] {"ip", "addr", "add", m_IPAddr, "dev", "eth0"});
				logger.info("Add ip: " + m_IPAddr);
			} catch (IOException e) {
				logger.warn("Add IP failure: " + e.getMessage(), e);
			}
		}
	}
	
}
